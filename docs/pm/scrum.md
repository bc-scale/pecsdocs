---
id: scrum
title: SCRUM
sidebar_label: SCRUM
---


```
  <h4 class="no_toc">Wireframes, se enfocan en:</h4>
 * Los tipos de información que será mostrada
 * La cantidad de las funciones disponibles
 * Las prioridades relativas de la información y las funciones
 * Las reglas para mostrar ciertos tipos de información
```


Un wireframe, también conocido como un esquema de página o plano de pantalla, es una guía visual que representa el
 esqueleto o estructura visual de un sistema web.

El wireframe esquematiza el diseño de página u ordenamiento del contenido del la apliación web, incluyendo elementos
 de la interfaz y sistemas de navegación, y cómo funcionan en conjunto.
 
Usualmente este esquema carece de estilo tipográfico, color o aplicaciones gráficas, ya que su principal objetivo
 reside en la funcionalidad, comportamiento y jerarquía de contenidos. En otras palabras, se enfoca en “qué hace la
  pantalla, no cómo se ve.”
  
Los esquemas pueden ser dibujados con lápiz y papel o esquemas en una pizarra, o pueden ser producidos con medios de diseño de aplicaciones de software libre o comerciales.


El wireframe del sitio web conecta la estructura conceptual, o arquitectura de la información, con la superficie, o diseño visual del sitio web.

Los wireframes ayudan a establecer funcionalidad, y las relaciones entre las diferentes plantillas de pantallas de un
 sitio web. Un proceso iterativo de creación de wireframes es una forma efectiva de hacer prototipos de páginas rápidos, mientras se mide la practicidad de un concepto de diseño. Típicamente, la esquematización comienza entre
  “diagramas de flujo de estructuras de trabajo de alto nivel o mapas de sitio y diseños de pantallas.” Dentro del
   proceso de construcción de un sitio web o apliación web, el dibujo de un wireframe es donde el concepto se vuelve
    tangible.

Aparte de los sitios web o aplicaciones web, los wireframes son usados para hacer prototipos de apliaciones móviles
, aplicaciones para
 ordenador u otros productos basados en pantalla que impliquen interacción hombre-máquina.
 
Las tecnologías futuras y los medios forzarán a los wireframes a adaptarse y evolucionar.

#### Wireframe Aplicación
Para la aplicación se ha desarrollado los siguientes wireframes:
<div class="row mb-4">
  <div class="col-12 text-center">
    Wireframe apliación
    {% asset diseno/wire/wireframe-app.png class="border mt-1 mb-1 mw-100" alt="WBS" %}
    
  </div>
</div>

{{site.alert.note}}
  [Definición Completa en Wikipedia][]
{{site.alert.end}}

[Definición Completa en Wikipedia]: https://es.wikipedia.org/wiki/Estructura_de_descomposici%C3%B3n_del_trabajo

