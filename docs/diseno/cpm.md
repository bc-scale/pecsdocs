---
id: cpm
title: Ruta Crítica
sidebar_label: Ruta Crítica (Critical Path Method - CPM)
---

title: Ruta Crítica
short-title: Ruta Crítica
description: Ruta Crítica (Critical Path Method - CPM)
diff2html: true


{% assign api = site.api | append: '/flutter' -%}
{% capture code -%} {{site.repo.this}}/tree/{{site.branch}}/src/_includes/code {%- endcapture -%}
{% capture examples -%} {{site.repo.this}}/tree/{{site.branch}}/examples {%- endcapture -%}
{% assign rawExFile = 'https://raw.githubusercontent.com/flutter/website/master/examples' -%}
{% capture demo -%} {{site.repo.flutter}}/tree/{{site.branch}}/dev/integration_tests/flutter_gallery/lib/demo {%- endcapture -%}

<style>dl, dd { margin-bottom: 0; }</style>

{{site.alert.secondary}}
  <h4 class="no_toc">Ruta Crítica</h4>

  * Organiza y prioriza el trabajo a realizar.
  * Visualiza el camino crítico.
  * Especifica los entregables del proyecto.
{{site.alert.end}}



<div class="row mb-4">
  <div class="col-12 text-center">
    {% asset diseno/WBS-Plataforma-REACT.png class="border mt-1 mb-1 mw-100" alt="WBS" %}
    
  </div>
</div>

Task dependencies are very important and, as mentioned in the previous lesson, they affect schedules and planning techniques. Critical Path Method (CPM) charts are an example of such a task planning method. A CPM chart is a visual way to organize task dependencies.
Creating a CPM Chart
The steps involved in making a CPM chart are:
1. Make a list of tasks needed to finish the project and then create a Work Breakdown Structure (WBS) for those tasks (see the Work Breakdown Structure lesson in Module 1: Introduction to Planning of this course).
2. Add time estimates to each task.
3. Organize the tasks by grouping dependencies horizontally together. For example, a task that is finish-start dependent on another should be grouped next to the task it is dependent on. Arrows can then be used depending on which task dependency is at work to depict the dependency (see Task Dependencies lesson for how to graphically represent different types of task dependencies).

{{site.alert.note}}
  [Definición Completa en Wikipedia][]
{{site.alert.end}}

[Definición Completa en Wikipedia]: https://es.wikipedia.org/wiki/M%C3%A9todo_de_la_ruta_cr%C3%ADtica

