---
id: wbs
title: Work Breakdown Structure
sidebar_label: Work Breakdown Structure
---

title: Work Breakdown Structure
short-title: WBS
description: Work Breakdown Structure.
diff2html: true


{% assign api = site.api | append: '/flutter' -%}
{% capture code -%} {{site.repo.this}}/tree/{{site.branch}}/src/_includes/code {%- endcapture -%}
{% capture examples -%} {{site.repo.this}}/tree/{{site.branch}}/examples {%- endcapture -%}
{% assign rawExFile = 'https://raw.githubusercontent.com/flutter/website/master/examples' -%}
{% capture demo -%} {{site.repo.flutter}}/tree/{{site.branch}}/dev/integration_tests/flutter_gallery/lib/demo {%- endcapture -%}

<style>dl, dd { margin-bottom: 0; }</style>

{{site.alert.secondary}}
  <h4 class="no_toc">Work Breakdown Structure</h4>

  * Organiza y prioriza el trabajo a realizar.
  * Visualiza el camino crítico.
  * Especifica los entregables del proyecto.
{{site.alert.end}}



<div class="row mb-4">
  <div class="col-12 text-center">
    {% asset diseno/WBS-Plataforma-REACT.png class="border mt-1 mb-1 mw-100" alt="WBS" %}
    <small>Click derecho sobre la imagen y seleccione: <b>Abrir la imagen en nueva pestaña</b> para ver la imágen en
     tamaño
     real</small>
  </div>
</div>

Una estructura de descomposición del trabajo (EDT), también conocida por su nombre en inglés Work Breakdown Structure o WBS, es una herramienta fundamental que consiste en la descomposición jerárquica, orientada al entregable, del trabajo a ser ejecutado por el equipo de proyecto, para cumplir con los objetivos de éste y crear los entregables requeridos, donde cada nivel descendente de la EDT representa una definición con un detalle incrementado del trabajo del proyecto.
  
El propósito de una EDT es organizar y definir el alcance total aprobado del proyecto según lo declarado en la documentación vigente. Su forma jerárquica permite una fácil identificación de los elementos finales, llamados "Paquetes de Trabajo". Se trata de un elemento exhaustivo en cuanto al alcance del proyecto, y sirve como base para la planificación del proyecto. Todo trabajo del proyecto debe poder rastrear su origen en una o más entradas de la EDT.:

{{site.alert.note}}
  [Definición Completa en Wikipedia][]
{{site.alert.end}}

[Definición Completa en Wikipedia]: https://es.wikipedia.org/wiki/Estructura_de_descomposici%C3%B3n_del_trabajo

