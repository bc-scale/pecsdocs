---
id: designdocstemplate_es
title: Design Docs - Español
sidebar_label: Design Docs ES
---

# Template para uso de Design Docs en Español.

{{site.alert.secondary}}
  <h4 class="no_toc">Design Doc</h4>

  * Cambia de nombre al Template.
  * Escribe tu diseño.
  * No sobre escribas este template.
{{site.alert.end}}

### RESUMEN

Coloque una breve introducción a su discusión o diseño aquí.

##### Autor: Nicolai Abruzzese Aguirre (GitHubTodoOpen)
##### Creado: 05/2020   /  Ultima Actualización: 05/2020

### OBJETIVOS

Objetivos de la discusión o diseño.

### ANTECEDENTES

Antecedentes necesarios para comprender el dominio del problema. No incluya ninguna "solución" en esta sección.

#### Glosario

Términos relevantes para la discusión: aquí encontrará un conjunto mínimo de definiciones de términos necesarios para comprender el dominio del problema. No incluya conceptos ampliamente conocidos (por ejemplo, no defina "URL" o "GUI"), solo las cosas necesarias para comprender la discusión.

### DISEÑO GENERAL

Descripción general del diseño o discusión.

#### Exluyentes

Lo que específicamente no se aborda en esta discusión o diseño.

### DETALLE DISEÑO/DISCUSION

Diseño detallado. Discutir.

### PREGUNTAS

* ¿Funcionará?

### TEST PLAN

Proporcione una descripción de las pruebas o un enlace a un plan de pruebas aquí, si la discusión involucra algo que se puede probar.

### PLAN DE MIGRACION

Proporcione una descripción del plan de migración o un enlace a un plan de migración aquí, si la discusión involucra algo que debe migrarse.

{{site.alert.note}}
  [Enlace al documento original][]
{{site.alert.end}}

[Enlace al documento original]: https://docs.google.com/document/d/1uSWvbq2HWagRGIfVhp5USGkZDK4Hpr1GYa06gFrDcjg/edit?usp=sharing

