---
id: reqs
title: Requerimientos
sidebar_label: REQS
---

```
  <h4 class="no_toc">Documento de Requerimientos</h4>

  * Reuniones
  * Elicitación de requerimientos
  * Documento de requerimientos del cliente.
```


#### Reuniones

Ninguna ya que es un proyecto interno

#### Elicitación de requerimientos

##### Definición

La actividad de obtener requisitos es un proceso interactivo e investigativo, que ocurre cuando se reúne con el cliente y los usuarios.

Los clientes a menudo tienen ideas sobre las características que desearían en un producto y cómo deberían ser estas características. Sin embargo, muchos clientes tienen un conocimiento limitado de cómo se construye el software e ideas vagas sobre lo que hace que un proyecto sea exitoso. Entonces puede ser difícil para los clientes comprender lo que realmente requieren en un producto. El rol del gerente de producto de software es ayudar al cliente a descubrir qué es lo que "quiere" y lo que "necesita".

Un "deseo" suele ser una funcionalidad o característica que el cliente desea que tenga el producto, que puede agregar valor, pero no necesariamente es esencial para el producto en sí. Una "necesidad", por otro lado, es una funcionalidad central que el producto debe tener para cumplir con el propósito del producto. Las necesidades deben tener prioridad en el desarrollo de productos.

La mejor manera de descubrir y desarrollar "necesidades" y "deseos" con su cliente es mediante la obtención de requisitos, donde entabla una discusión sobre el producto con su cliente. A través de la discusión, el gerente y el equipo del producto de software pueden ayudar a proporcionar información sobre lo que el cliente "necesita" y "quiere" que haga el producto, y cómo estos objetivos pueden alcanzarse de manera factible. Puede ser que la visión inicial que el cliente tenía del producto haya cambiado, pero a través de la participación con el equipo del producto de software, cualquier cambio debería ser proactivo y el cliente debería sentirse tranquilo de que el equipo comprende las necesidades de los usuarios.

Tenga en cuenta que la obtención de requisitos como "necesidades" y "deseos" no significa necesariamente que todas las características e ideas del cliente que entran en la categoría "querer" no sean factibles o deban descartarse. De hecho, estas ideas pueden ser muy buenas. Pero el rol del gerente de producto de software es asegurarse de que los objetivos sean factibles, las expectativas del cliente sean realistas y el producto producido sea el mejor resultado posible.

##### Elicitación de Requerimeintos del Proyecto

Platfaorm REACT es un software base, el cual tendra caracteristicas escalables para poder desarrollar cualquier tipo de software y acortar el tiempo de desarrollo.

La platforma debe contener las siguientes caracterpisticas:

* Propuesta
* Diseño del proyecto
  * Work Breakdown Structure - WBS
  * Ruta Crítica - CPM
  * UI/UX
    * Wireframes
    * Mockups
* Documentación
  * Requerimientos del cliente
    * Epicas
  * PM/SCRUM
  * User Stories (Backlog)
  * Release Planing


#### Requerimientos del cliente

Las Epicas, son historias de usuario de gran tamaño, que agrupan una cantidad de funcionalidades o representan un grupo de tareas.

El proyecto contiene las siguientes Epicas, estas pueden cambiar o pueden agregarse aun mas segun el crecimeinto del proyecto.

##### Epicas

* **Plataforma**, funcionalidades y tareas para el desarrollo de la plataforma base.

* **Seguridad**P, funciones de autenticacion, autorizacion, encriptacion de la apliación.

* **Usuarios**, administracion de usuarios del software.

* **UI/UX**, Interfaz grafica del usuario

* **Configuración**, de la apliación e integración con otros servicios, uso de librerías.

* **Diseño y Documentación**, tareas de diseño y documentación adicional del proyecto y apliación.

#### PM/SCRUM

Las tareas de Project Management y SCRUM son administradas en el sistema de tickets:

{{site.alert.note}}
Sistema de Tickets: http://192.168.86.30:8080
{{site.alert.end}}

#### User Stories

Las tareas del Backlog son administradas en el sistema de tickets:

{{site.alert.note}}
Sistema de Tickets: http://192.168.86.30:8080
{{site.alert.end}}

#### Release Planning

Las tareas de Release Planning son administradas en el sistema de tickets:

{{site.alert.note}}
Sistema de Tickets: http://192.168.86.30:8080
{{site.alert.end}}


