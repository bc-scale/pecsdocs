---
id: sistemabase
title: Sistema Base
sidebar_label: Sistema Base
---

  <h4 class="no_toc">Sistema Base</h4>

  * Como esta conformado el Sistema Base.
  * Versiones.
  * Desiciones sobre esta base.


### RESUMEN

Se ha tomadoen cuenta el template creative-tim.com Dashboard-pro, para uso en toda la apliación.

##### Autor: Nicolai Abruzzese Aguirre (GitHubTodoOpen)
##### Creado: 05/2020   /  Ultima Actualización: 05/2020

### OBJETIVOS

Especificar el diseño, componentes, framewirks para una apliación react.

### ANTECEDENTES

La plataforma base debe tener capacidades para soportar todo tipo de logica de negocio, debe incuir las funcionalidades comerciales de base y descritas en el diagrama de Work Breakdown Structure (WBS).

#### Glosario

* **Platafroma Base:** Software base con capacidades mínimas para administrar usuarios, permisos, con una interfaz grafica capaz de su administración y despliegue en algun servidor en la nube.

### DISEÑO GENERAL

Se ha tomado React como base del proyecto, por su elegancia y flexibilidad, en conjunto con material-ui de google para el frontend, usando creative-tim.com Material UI - Dashboard PRO, como base del frontend, Google FireStore como sistema de documentos para almacenamiento persistente.

#### Exluyentes

En la primera etapa de este proyecto solo se concentra en crear una apliación base lista para deployment.

### DETALLE DISEÑO/DISCUSION

#### Plataforma y sus componentes

Se ha tomado como base de este proyecto las siguientes versiones:
Archivo: [package.json](http://localhost:5000/docs/documentos/internos/designdocs/sistema-base)

```
"dependencies": {
    "@material-ui/core": "4.3.2",
    "@material-ui/icons": "4.2.1",
    "chartist": "0.10.1",
    "firebase": "^7.14.5",
    "history": "4.9.0",
    "moment": "2.24.0",
    "node-sass": "4.12.0",
    "nouislider": "14.0.2",
    "perfect-scrollbar": "1.4.0",
    "react": "16.9.0",
    "react-big-calendar": "0.22.0",
    "react-bootstrap-sweetalert": "4.4.1",
    "react-chartist": "0.13.3",
    "react-datetime": "2.16.3",
    "react-dom": "16.9.0",
    "react-google-maps": "9.4.5",
    "react-jvectormap": "0.0.12",
    "react-router-dom": "5.0.1",
    "react-scripts": "3.1.0",
    "react-swipeable-views": "0.13.3",
    "react-table": "6.10.0",
    "react-tagsinput": "3.19.0"
  },
```

#### Componentes

##### Branding
* Utilizar variables estaticas en archivo, evitar el hardcode en frontend
* Implementar logo como imagen y texto
* Footer, pagina web, direccion, telefono, email de contacto, redes sociales, twitter, fb, instagram, tiktok

##### Autenticacion

###### Base de datos

* **Firebase**: Un sistema NoSQL, manejo de documentos, _firestore_ para manejo de credeciales con soporte email/password del propio _firestore_
  * El servicio es gratuito hasta un limite, requiere usuario registrado en firebase.
* Se utilizara el servicio de firestore de autenticacion email/pass inicialmente.
* Implementar modo desarrollo y producción.

###### UI/UX

* **Autenticación:** Las pantallas deben mostrar, una landingpge cuando no se esta loggeado y el panel administradro cuando si se esta.

  * **Login**, pantalla de login, cuendo la operacion tiene exito, desplegar en pantalla el nombre de usuario o correo electrónico.
  * **Logout**, pantalla de logout, el boton solo se muestra en pantalla cuando existe una sesion en curso.

###### Usuarios

* Usuario con id autogenerado en almacenamiento
* El usuario solo cuenta con su id, correo electronico como username y clave
* La informacion del usuario debe almacenarse en Perfil del usuario 

### DevOps

#### Diseño del pipeline
TBD

#### Implementación
TBD

### TEST PLAN

Realizar un test plan adecuado, planificarlo en Jira, y publicar los hallazgos.



{{site.alert.note}}
  [Enlace al documento original][]
{{site.alert.end}}

[Enlace al documento original]: https://docs.google.com/document/d/1uSWvbq2HWagRGIfVhp5USGkZDK4Hpr1GYa06gFrDcjg/edit?usp=sharing

