---
id: basededatos
title: Base de Datos
sidebar_label: Base de Datos
---
```
  <h4 class="no_toc">Base de Datos</h4>
  * Cambia de nombre al Template.
  * Escribe tu diseño.
  * No sobre escribas este template.
```

### SUMMARY

Place a very short introduction to your discussion or design here.

##### Author: {Author Name} ({GitHub ID})
##### Go Link: flutter.dev/go/{document-title}
##### Created: {Month}/{Year}   /  Last updated: {Month}/{Year}

### OBJECTIVE

Objectives of the discussion or design.

### BACKGROUND

Background needed to understand the problem domain. Don’t include any “solutions” in this section.

#### Glossary

Terms Relevant to Discussion - A minimal set of definitions of terms needed to understand the problem domain go here. Do not include widely known concepts (e.g. don’t define “URL” or “GUI”), just things needed to understand the discussion.

### OVERVIEW

Overview of the design or discussion.

#### Non-goals

What is specifically not being addressed by this discussion or design.

### DETAILED DESIGN/DISCUSSION

Detailed Design. Discuss.

### OPEN QUESTIONS

* Will it work?

### TESTING PLAN

Provide a description of testing or a link to a testing plan here, if the discussion involves something that can be tested.

### MIGRATION PLAN

Provide a description of the migration plan or a link to a migration plan here, if the discussion involves something that must be migrated.

{{site.alert.note}}
  [Enlace al documento original][]
{{site.alert.end}}

[Enlace al documento original]: https://docs.google.com/document/d/1uSWvbq2HWagRGIfVhp5USGkZDK4Hpr1GYa06gFrDcjg/edit?usp=sharing

