---
id: interfaz-grafica
title: Interfaz Gráfica
sidebar_label: Interfaz Gráfica
---
 ```
  <h4 class="no_toc">Interfaz Gráfica</h4>
  * Cambia de nombre al Template.
  * Escribe tu diseño.
  * No sobre escribas este template.
```


### SUMMARY

SideNav, menu items on the left side of the app.

##### Author: Nicolai Abruzzese (GitHubTodoOpen)
##### Go Link: flutter.dev/go/{document-title}
##### Created: 05/2020   /  Last updated: 05/2020

### OBJECTIVE

Objectives of the discussion or design.

### BACKGROUND

Background needed to understand the problem domain. Don’t include any “solutions” in this section.

#### Glossary

Terms Relevant to Discussion - A minimal set of definitions of terms needed to understand the problem domain go here. Do not include widely known concepts (e.g. don’t define “URL” or “GUI”), just things needed to understand the discussion.

### OVERVIEW

Overview of the design or discussion.

#### Non-goals

What is specifically not being addressed by this discussion or design.

### DETAILED DESIGN/DISCUSSION

Detailed Design. Discuss.

### OPEN QUESTIONS

* Will it work?

### TESTING PLAN

Provide a description of testing or a link to a testing plan here, if the discussion involves something that can be tested.

### MIGRATION PLAN

Provide a description of the migration plan or a link to a migration plan here, if the discussion involves something that must be migrated.

{{site.alert.note}}
  [Enlace al documento original][]
{{site.alert.end}}

[Enlace al documento original]: https://docs.google.com/document/d/1uSWvbq2HWagRGIfVhp5USGkZDK4Hpr1GYa06gFrDcjg/edit?usp=sharing

